Run `./gradlew jmh` to run all the benchmarks. It will take hours.

Here's a recent run:

```
Benchmark                                                                                                                       Mode  Samples         Score        Error  Units
o.m.m.r.j.ExponentiallyDecayingReservoirBenchmark.readWhileConcurrentRecording                                                 thrpt      200   5920131.976 ±  81808.299  ops/s
o.m.m.r.j.ExponentiallyDecayingReservoirBenchmark.readWhileConcurrentRecording:readSnapshotsForConcurrentMeasurements          thrpt      200         9.425 ±      0.045  ops/s
o.m.m.r.j.ExponentiallyDecayingReservoirBenchmark.readWhileConcurrentRecording:recordConcurrentMeasurements                    thrpt      200   5920122.551 ±  81808.294  ops/s
o.m.m.r.j.ExponentiallyDecayingReservoirBenchmark.readWhileSingleThreadedRecording                                             thrpt      200   6155887.888 ±  35955.493  ops/s
o.m.m.r.j.ExponentiallyDecayingReservoirBenchmark.readWhileSingleThreadedRecording:readSnapshotsForSingleThreadMeasurements    thrpt      200         9.346 ±      0.006  ops/s
o.m.m.r.j.ExponentiallyDecayingReservoirBenchmark.readWhileSingleThreadedRecording:recordSingleThreadMeasurements              thrpt      200   6155878.543 ±  35955.493  ops/s
o.m.m.r.j.HdrHistogramReservoirBenchmark.readWhileConcurrentRecording                                                          thrpt      200   8506106.691 ± 125349.187  ops/s
o.m.m.r.j.HdrHistogramReservoirBenchmark.readWhileConcurrentRecording:readSnapshotsForConcurrentMeasurements                   thrpt      200         9.293 ±      0.009  ops/s
o.m.m.r.j.HdrHistogramReservoirBenchmark.readWhileConcurrentRecording:recordConcurrentMeasurements                             thrpt      200   8506097.398 ± 125349.186  ops/s
o.m.m.r.j.HdrHistogramReservoirBenchmark.readWhileSingleThreadedRecording                                                      thrpt      200  21314118.481 ± 107056.559  ops/s
o.m.m.r.j.HdrHistogramReservoirBenchmark.readWhileSingleThreadedRecording:readSnapshotsForSingleThreadMeasurements             thrpt      200         9.314 ±      0.008  ops/s
o.m.m.r.j.HdrHistogramReservoirBenchmark.readWhileSingleThreadedRecording:recordSingleThreadMeasurements                       thrpt      200  21314109.167 ± 107056.561  ops/s
o.m.m.r.j.SlidingTimeWindowReservoirBenchmark.readWhileConcurrentRecording                                                     thrpt      200   1857099.390 ±  94367.026  ops/s
o.m.m.r.j.SlidingTimeWindowReservoirBenchmark.readWhileConcurrentRecording:readSnapshotsForConcurrentMeasurements              thrpt      200         2.125 ±      0.102  ops/s
o.m.m.r.j.SlidingTimeWindowReservoirBenchmark.readWhileConcurrentRecording:recordConcurrentMeasurements                        thrpt      200   1857097.265 ±  94366.973  ops/s
o.m.m.r.j.SlidingTimeWindowReservoirBenchmark.readWhileSingleThreadedRecording                                                 thrpt      200   1953661.172 ±  60174.687  ops/s
o.m.m.r.j.SlidingTimeWindowReservoirBenchmark.readWhileSingleThreadedRecording:readSnapshotsForSingleThreadMeasurements        thrpt      200         2.553 ±      0.063  ops/s
o.m.m.r.j.SlidingTimeWindowReservoirBenchmark.readWhileSingleThreadedRecording:recordSingleThreadMeasurements                  thrpt      200   1953658.620 ±  60174.644  ops/s
o.m.m.r.j.SlidingWindowReservoirBenchmark.readWhileConcurrentRecording                                                         thrpt      200  19052903.567 ± 288635.068  ops/s
o.m.m.r.j.SlidingWindowReservoirBenchmark.readWhileConcurrentRecording:readSnapshotsForConcurrentMeasurements                  thrpt      200         0.695 ±      0.017  ops/s
o.m.m.r.j.SlidingWindowReservoirBenchmark.readWhileConcurrentRecording:recordConcurrentMeasurements                            thrpt      200  19052902.872 ± 288635.062  ops/s
o.m.m.r.j.SlidingWindowReservoirBenchmark.readWhileSingleThreadedRecording                                                     thrpt      200  38072842.239 ± 461911.942  ops/s
o.m.m.r.j.SlidingWindowReservoirBenchmark.readWhileSingleThreadedRecording:readSnapshotsForSingleThreadMeasurements            thrpt      200         0.705 ±      0.007  ops/s
o.m.m.r.j.SlidingWindowReservoirBenchmark.readWhileSingleThreadedRecording:recordSingleThreadMeasurements                      thrpt      200  38072841.534 ± 461911.937  ops/s
o.m.m.r.j.UniformReservoirBenchmark.readWhileConcurrentRecording                                                               thrpt      200  42525354.782 ± 609764.276  ops/s
o.m.m.r.j.UniformReservoirBenchmark.readWhileConcurrentRecording:readSnapshotsForConcurrentMeasurements                        thrpt      200         9.349 ±      0.006  ops/s
o.m.m.r.j.UniformReservoirBenchmark.readWhileConcurrentRecording:recordConcurrentMeasurements                                  thrpt      200  42525345.432 ± 609764.277  ops/s
o.m.m.r.j.UniformReservoirBenchmark.readWhileSingleThreadedRecording                                                           thrpt      200  35557702.144 ± 606733.633  ops/s
o.m.m.r.j.UniformReservoirBenchmark.readWhileSingleThreadedRecording:readSnapshotsForSingleThreadMeasurements                  thrpt      200         9.327 ±      0.007  ops/s
o.m.m.r.j.UniformReservoirBenchmark.readWhileSingleThreadedRecording:recordSingleThreadMeasurements                            thrpt      200  35557692.816 ± 606733.633  ops/s
```
