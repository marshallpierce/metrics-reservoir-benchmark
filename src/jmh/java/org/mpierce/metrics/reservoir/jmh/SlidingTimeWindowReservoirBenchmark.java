package org.mpierce.metrics.reservoir.jmh;

import com.codahale.metrics.Reservoir;
import com.codahale.metrics.SlidingTimeWindowReservoir;
import com.codahale.metrics.Snapshot;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Group;
import org.openjdk.jmh.annotations.GroupThreads;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import java.util.SplittableRandom;
import java.util.concurrent.TimeUnit;

public class SlidingTimeWindowReservoirBenchmark {

    @State(Scope.Group)
    public static class GroupState {
        final Reservoir reservoir = new SlidingTimeWindowReservoir(1, TimeUnit.SECONDS);
    }

    @State(Scope.Thread)
    public static class ThreadState {
        final SplittableRandom random = new SplittableRandom();
    }

    @Benchmark
    @Group("readWhileConcurrentRecording")
    @GroupThreads(2)
    public void recordConcurrentMeasurements(GroupState groupState, ThreadState threadState) {
        groupState.reservoir.update(threadState.random.nextLong(1_000_000_000));
    }

    @Benchmark
    @Group("readWhileConcurrentRecording")
    public Snapshot readSnapshotsForConcurrentMeasurements(GroupState groupState) throws InterruptedException {
        // don't really care about the performance of reading much as it's allocation-heavy and boring.
        // Just want to perturb writing now and then to be representative.
        Thread.sleep(100);
        return groupState.reservoir.getSnapshot();
    }

    @Benchmark
    @Group("readWhileSingleThreadedRecording")
    @GroupThreads(1)
    public void recordSingleThreadMeasurements(GroupState groupState, ThreadState threadState) {
        groupState.reservoir.update(threadState.random.nextLong(1_000_000_000));
    }

    @Benchmark
    @Group("readWhileSingleThreadedRecording")
    public Snapshot readSnapshotsForSingleThreadMeasurements(GroupState groupState) throws InterruptedException {
        Thread.sleep(100);
        return groupState.reservoir.getSnapshot();
    }
}
